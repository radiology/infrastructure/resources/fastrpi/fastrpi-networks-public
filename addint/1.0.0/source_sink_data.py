def get_source_data():
    source_data = {
            'left_hand': {'s1': 4, 's2': 5, 's3': 6, 's4': 7},
            'right_hand': {'s1': 1, 's2': 3, 's3': 3, 's4': 7},
            }
    return source_data
    
def get_sink_data():
    sink_data = {'sum': 'vfs://output/fastr_result_{sample_id}.txt'}
    return sink_data
